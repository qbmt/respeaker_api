# Respeaker API

>This is an api based on express js.

The api uses a local json file as database. The file is made when the program is first run.

## Installation

```bash
npm install
```

## Run

```bash
npm start
```

## Routes

### Get

Get all messages

`GET /api/messages`

### Post

Create new data
`POST /API/MESSAGES`

```json
{
"sender": "string",
"confidence": "string",
"message": "string",
"understood": "boolean"
}
```

Example with real world test data.

```json
{
"sender": "zora",
"message": "Ow hello who are you",
"understood": true,
"confidence": "0,987654"
}
```

### Delete

`DELETE /API/MESSAGES/DELETE/ALL`

```json
{
    "message": "Messages deleted successfully!"
}
```

## Meta

Arthur Verstraete - [ArthurVerstraete](https://github.com/ArthurVerstraete) - arthur.verstraete@student.vives.be

Frederik Feys – [FrederikFeys](https://github.com/FrederikFeys) – frederik.feys@student.vives.be
