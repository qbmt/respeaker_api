const Joi = require('joi');
let fs = require('fs');
const { uuid } = require('uuidv4');
 
const schema = Joi.object().keys({
    sender: Joi.string().required(),
    message: Joi.string().required(),
    confidence: Joi.string(),
    understood: Joi.boolean()
});
 
function create(message, req, res) { 
    const result = Joi.validate(message, schema);
    if (result.error == null) {
        message.createdAt = new Date();
        message._id = uuid().replace(/-/gi, '');

        fs.open('./data/messages.json', 'r+', (err, fd) => {
            fs.readFile('./data/messages.json', function (err, data) {
                let json = JSON.parse(data);
                json.push(message);
    
                fs.writeFile('./data/messagesTemp.json', JSON.stringify(json), function (err) {
                    if (err) {
                        throw err;
                    } else {
                        console.log('added message to temporary database');
                        fs.readFile('./data/messagesTemp.json', function (err, data) {
                            fs.writeFile('./data/messages.json', data, function (err) {
                                if (err) {
                                    throw err;
                                } else {
                                    console.log('added message to database');
                                    console.log(message);
                                    res.json(message);
                                }
                            })
                        })
                    }
                })
                json = [];
            })
            fs.close(fd, () => {})
        })
    } else {
        console.log('could not add message to database');
        res.json({ERROR: "COULD NOT ADD MESSAGE TO DATABASE"});
    }
}

function deleteAll(req, res) {
    fs.writeFile('./data/messages.json', JSON.stringify([]), function(err){
        if (err) {
            res.json({ERROR: "COULD NOT DELETE MESSAGES"})
            throw err;
        } else {
            console.log('deleted everything from database');
        }
    })
}
 
module.exports = {
    create,
    deleteAll
};