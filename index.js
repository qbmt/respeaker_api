const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const fs = require('fs');
const dotenv = require('dotenv');
dotenv.config();

const { exec } = require("child_process");
const messages = require('./db/messages');

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const file = './data/messages.json'
const dir = './data/'

try {
  if (fs.existsSync(file)) {
    console.log("The file " + file + " already exist");
  } else if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir, function (err) {
      if (err) {
        console.log(err)
      } else {
        console.log("New directory" + dir + " successfully created.")
      }
    });
    fs.writeFile(file, "[]", function (err) {
      if (err) throw err;
      console.log(file + " is created successfully.");
    })
  } else if (fs.existsSync(dir)) {
    fs.writeFile(file, "[]", function (err) {
      if (err) throw err;
      console.log(file + " is created successfully.");
    })
  } else {
    console.error("No file created")
  }
} catch (err) {
  console.error(err)
}

app.get('/api/messages', (req, res) => {
  if (req.query.key == process.env.API_KEY) {
    fs.readFile('./data/messages.json', function (err, data) {
      console.log('get messages from database');
      res.json(JSON.parse(data));
    });
  } else {
    res.json({
      ERROR: 'Wrong API key'
    });
  }
});

app.post('/api/messages', (req, res) => {
  if (req.query.key == process.env.API_KEY) {
    messages.create(req.body, req, res);
  } else {
    res.json({
      ERROR: 'Wrong API key'
    });
  }
});

app.post('/api/robot_address', (req, res) => {
  if (req.query.key == process.env.API_KEY) {
    console.log(req.body);
    fs.readFile("../respeaker-server/.env", 'utf8', function (err, data) {
      if (err) {
        return console.log(err);
      }
      var result = data.replace(/ROBOT_ADDRESS=.*/g, "ROBOT_ADDRESS=" + req.body.robot_address);

      fs.writeFile("../respeaker-server/.env", result, 'utf8', function (err) {
        if (err) return console.log(err);
      });
    });

    exec("sudo reboot", (error, stdout, stderr) => {
      if (error) {
          console.log(`error: ${error.message}`);
          return;
      }
      if (stderr) {
          console.log(`stderr: ${stderr}`);
          return;
      }
      console.log(`stdout: ${stdout}`);
  });
  } else {
    res.json({
      ERROR: 'Wrong API key'
    });
  }
});

app.delete('/api/messages/all', (req, res) => {
  if (req.query.key == process.env.API_KEY) {
    messages.deleteAll(req, res);
  } else {
    res.status(500).send({
      ERROR: "Wrong API key"
    })
  }
});

const port = process.env.PORT || 4000;
app.listen(port, () => {
  console.log(`listening on ${port}`);
});