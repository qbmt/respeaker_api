import random

chars = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
key = ''
filename = "../key.txt"
serverfile = "../respeaker-server/.env"
sitefile = "../respeaker_site/.env"
apifile = './.env'

for x in range(0, 15, 1):
    i = random.randint(0, len(chars))
    key = key + chars[i-1]

print(key)

keyfile = open(filename, 'w')
keyfile.write(key)
keyfile.close()

keyfile = open(serverfile, 'a')
keyfile.write("\nAPI_KEY=" + key)
keyfile.close()

keyfile = open(sitefile, 'a')
keyfile.write("\nREACT_APP_API_KEY=" + key)
keyfile.close()

keyfile = open(apifile, 'a')
keyfile.write("API_KEY=" + key)
keyfile.close()